(function($) {
  'use strict';

  /* ==========================================================================
    DEVICE CAROUSEL
  ========================================================================== */

  var $overviewSlide = $('.owl-carousel')
  if ($overviewSlide.length > 0) {
    $overviewSlide.owlCarousel({
      loop: true,
      center: true,
      margin: 0,
      items: 1,
      nav: false,
      dots: true,
      dotsContainer: '.dots'
    })
    $('.owl-dot').on('click', function () {
      $(this).addClass('active').siblings().removeClass('active');
      $overviewSlide.trigger('to.owl.carousel', [$(this).index(), 300]);
    });
  }

}) (jQuery);

